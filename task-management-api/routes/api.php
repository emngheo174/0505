<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserProjectController;
use App\Http\Controllers\UserTaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Router for User controller
 */
Route::group(['middleware' => ['web']], function () {
    Route::get('user', [UserController::class, 'index']);
    Route::get('user/show/{id}', [UserController::class, 'show']);
    Route::post('user/login', [UserController::class, 'login']);
    Route::post('user/update/{id}', [UserController::class, 'updateProfile']);
    Route::post('user/register', [UserController::class, 'register']);
    Route::post('user', [UserController::class, 'logout']);
});
Route::get('/users/search/{name}', [UserController::class, 'search']);

Route::get('/tasks', [TaskController::class , 'getAll']);
Route::post('/tasks', [TaskController::class, 'createTask']);
Route::put('/tasks/{task}', [TaskController::class, 'editTaskById']);
Route::get('/tasks/{task}', [TaskController::class , 'getTaskById']);
Route::get('/tasks/{task}/get-name', [TaskController::class, 'getUsers']);
Route::get('/tasks/{task}/get-project', [TaskController::class, 'getProject']);

/**
 * Router for Project controller
 */
Route::get('/projects', [ProjectController::class, 'getAll']);
Route::get('/project', [ProjectController::class, 'get']);
Route::get('/projects/{project}', [ProjectController::class, 'getProjectById']);
Route::post('/projects', [ProjectController::class, 'createProject']);
Route::put('/projects/{project}', [ProjectController::class, 'editProjectById']);
Route::get('/projects/{project}/get-name', [ProjectController::class, 'getNameUsers']);
Route::get('/projects/{project}/get-task', [ProjectController::class, 'getTasks']);
Route::delete('/projects/{project}/get-name', [ProjectController::class, 'deleteNameUsers']);
Route::get('/projects-done', [ProjectController::class, 'get']);

/**
 * Router for Task controller
 */
Route::post('/user-task/{id}', [UserTaskController::class, 'createUserTask']);

/**
 * Router for User-project controller
 */
Route::post('/users-project/{id}', [UserProjectController::class, 'addUser']);
Route::get('/users-project/{id}', [UserProjectController::class, 'findUser']);
Route::delete('/users-project/{project_id}', [UserProjectController::class, 'deleteUser']);