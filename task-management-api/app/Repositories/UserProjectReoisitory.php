<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

class UserProjectRepository extends BaseRepository{
        public function delete($id)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();

            return true;
        }
        return false;
    }
}