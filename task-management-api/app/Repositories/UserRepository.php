<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    // public function __construct(User $user)
    // {
    //     $this->user = new UserRepository($user);
    // }

    public function search($email)
    {
        $result = $this->model->where('email', 'LIKE', $email)->get();
        return $result;
    }
}
