<?php
namespace App\Repositories;
use App\Repositories\BaseRepository;
use PharIo\Manifest\License;
use App\Repositories\UserRepository;
use App\Models\User;
use Symfony\Component\CssSelector\Node\FunctionNode;

class ProjectRepository extends BaseRepository
{
    public function projectMaster()
    {
        return $this->model->where('status', '=', 0)->get();
    }
    
    public function project()
    {
        return $this->model->where('status', '=', 1)->get();
    }
}

