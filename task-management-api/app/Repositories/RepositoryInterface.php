<?php
namespace App\Repositories;

interface RepositoryInterface
{
    /**
     * Get all record from the table
     * 
     * @return array
     */
    public function getAll();

    /**
     * Create new records to the table
     * 
     * @param $data
     * @return array
     */
    public function create($data);

    /**
     * Update record to the table by id
     * 
     * @param $data
     * @param $id
     * @return boolean
     */
    public function update($data, $id);

    /**
     * Delete record in the table by id
     * 
     * @param $id
     * @return boolean
     */
    public function delete($id);

    /**
     * Show record in the table by id
     * 
     * @param $id
     * @return array
     */
    public function find($id);
}