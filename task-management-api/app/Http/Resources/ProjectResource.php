<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'status'=>$this->status,
            'date_start'=>$this->date_start,
            'date_finish'=> $this->date_finish,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
