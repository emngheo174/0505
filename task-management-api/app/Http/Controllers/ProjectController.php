<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Http\Resources\TaskResource;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Repositories\ProjectRepository;
use App\Repositories\UserRepository;
use SebastianBergmann\Environment\Console;
use App\Http\Resources\UserResource;
use App\Models\User;

class ProjectController extends Controller
{
    protected $project;

    /**
     * Declare constructor with Program model
     * 
     * @param Project
     * @param $project
     * @return object
     */
    public function __construct(Project $project)
    {
        $this->project = new ProjectRepository($project);
    }

    /**
     * Get all records from project master
     * 
     * @return array
     */
    public function getAll()
    {
        $projects = $this->project->projectMaster();
        return response()->json(ProjectResource::collection($projects));
    }

    /**
     * Get all records from all project attempt
     * 
     * @return array
     */
    public function get()
    {
        $projects = $this->project->project();
        return response()->json(ProjectResource::collection($projects));
    }

    /**
     * Get project by id
     * 
     * @return array
     */
    public function getProjectById($id)
    {
        $project = $this->project->find($id);
        return response()->json(new ProjectResource($project));
    }

    /** 
     * Get user name by id
     * 
     * @return array
     */    
    public function getNameUsers($id)
    {
        $project = $this->project->find($id);
        $res = $project->users;
        return response()->json(UserResource::collection($res));
    }

    /** 
     * Get task by id
     * 
     * @return array
     */  
    public function getTasks($id)
    {
        $project = $this->project->find($id);
        $tasks = $project->tasks;
        return response()->json(TaskResource::collection($tasks));
    }

    public function createProject(Request $request)
    {
        $this->project->create($request->all());
    }

    public function editProjectById(Request $request, $id)
    {
        $this->project->update($request->all(), $id);
    }
}
