<?php

namespace App\Http\Controllers;

use App\Models\UserTask;
use Illuminate\Http\Request;

class UserTaskController extends Controller
{
    /**
     * Create user join task by user id
     * 
     * @param $request
     * @param $id
     * @return array
     */
    public function createUserTask(Request $request, $id)
    {
        $users = $request->input();
        if (isset($users)) {
            foreach ($users as $user) {
                UserTask::create([
                    'user_id' => $user['id'],
                    'task_id' => $id
                ]);
            }
        }
    }
}
