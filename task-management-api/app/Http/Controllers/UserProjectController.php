<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserProjectReource;
use App\Models\UserProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class UserProjectController extends Controller
{
    /**
     * Add user to project
     * 
     * @param $request
     * @param $id
     * @return mixed
     */
    public function addUser(Request $request, $id)
    {
        $user = $request->input();
        UserProject::create([
            'user_id' => $user['id'],
            'project_id' => $id
        ]);
    }
    /**
     * Delete sort user 
     * 
     * @param $request
     * @param $id
     * @return mixed
     */
    public function deleteUser($id)
    {
        $result = UserProject::find($id);
        if ($result) {
            $result->delete();

            return true;
        }
        return false;
    }
    /**
     * Find user by project_id
     * 
     * @param $request
     * @param $id
     * @return mixed
     */
    public function findUser($project_id)
    {

        $result2 = DB::table('user_projects')->where('project_id', $project_id)->get();


        return response()->json(UserProjectReource::collection($result2));
    }
}
