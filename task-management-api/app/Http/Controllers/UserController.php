<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use League\Config\Exception\ValidationException;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Declare constructor with User model
     * 
     * @param Task
     * @param $task
     * @return object
     */
    protected $userRepo;
    public function __construct(User $user)
    {
        $this->userRepo = new UserRepository($user);
    }
    /**
     * Get all records from the User table
     * 
     * @return array
     */
    public function index()
    {
        $user = User::all();
        return response()->json(UserResource::collection($user));
    }
    /** 
     * User login
     * 
     * @return array
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        $email = $request->email;
        $password = $request->password;
        $request->except(['_token']);
        $user = DB::table('users')->where('email', $email)->first();
        $user = DB::table('users')->where('password', $password)->first();
        if ($user->password == $password) {
            return $user;
        } else {
            echo 'wrong';
        }
    }
    /** 
     * Create user
     * 
     * @return array
     */
    public function register(Request $request)
    {
        $data = $request->all();
        User::create($data);
        session()->flash('message', 'Your account is created');
    }
    /** 
     * Get user by id
     * 
     * @return array
     */
    public function show($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(new UserResource($user));
    }
    /** 
     * Update user by id
     * 
     * @return array
     */
    public function updateProfile(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->name !== $request->input('name')) {
            $user->name = $request->input('name');
        }
        if ($user->password !== $request->input('password')) {
            $user->password = $request->input('password');
        }
        $user->save();
        return response()->json(['User updated successfully.', new UserResource($user)]);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
    /** 
     * Get user by name
     * 
     * @return array
     */
    public function search($name)
    {
        $user = $this->userRepo->search($name);
        if (is_null($user)) {
            return response()->json('Data not found', 404);
        }
        return response()->json(UserResource::collection($user));
    }
  
}
