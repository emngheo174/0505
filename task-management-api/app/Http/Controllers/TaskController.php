<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Http\Resources\TaskResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\User;
use App\Repositories\TaskRepository;
use Psy\TabCompletion\Matcher\FunctionsMatcher;
use Symfony\Component\Console\Input\Input;

class TaskController extends Controller
{
    protected $task;

    /**
     * Declare constructor with Task model
     * 
     * @param Task
     * @param $task
     * @return object
     */
    public function __construct(Task $task)
    {
        $this->task = new TaskRepository($task);
    }

    /**
     * Get all records from the Task table
     * 
     * @return array
     */
    public function getAll()
    {
        $tasks = $this->task->getAll();
        return response()->json(TaskResource::collection($tasks));
    }

    public function getTaskById($id)
    {
        $task = $this->task->find($id);
        return response()->json(new TaskResource($task));
    }

    public function getUsers($id)
    {
        $task = $this->task->find($id);
        $userName = $task->users;
        return response()->json(UserResource::collection($userName));
    }

    public function getProject($id)
    {
        $task = $this->task->find($id);
        $project = $task->project;
        return response()->json(new ProjectResource($project));
    }

    public function createTask(Request $request)
    {
        $this->task->create($request->all());
    }

    public function editTaskById(Request $request, $id)
    {
        $this->task->update($request->all(), $id);
    }
}
