<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = "tasks";

    protected $fillable = ['name', 'description', 'status', 'project_id', 'date_start', 'date_finish'];

    /**
     * Relationship task table with user table
     * 
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_tasks');
    }

    /**
     * Relationship task table with project table
     * 
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
